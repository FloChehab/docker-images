# docker

Repo for hosting common docker related images (and Dockerfiles) I might use in my projects.

## Images

* [docker](./docker_ci/Dockerfile)

  Docker image to use docker (& docker-compose) in CI with basic tools already installed.

* [style_pre_commit](./style_pre_commit/Dockerfile)

  Docker image to use [pre-commit](https://pre-commit.com/) in CI (with python & node pre-installed).
